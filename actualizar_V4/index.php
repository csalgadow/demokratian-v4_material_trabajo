<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
?>
<!DOCTYPE html>
<html lang="es"><head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content=" ">
  <meta name="keywords" content=""/>
  <meta name="language" content="es">
  <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate">
  <meta http-equiv="pragma" CONTENT="no-cache">
  <meta http-equiv="Pragma" content="no-cache">
  <meta http-equiv="Expires" content="0">
        <title>Sistema de instalación de DEMOKRATIAN</title>
        <meta name="author" content="Carlos Salgado">
        <link rel="icon"  type="image/png"  href="../temas/demokratian/images/icono.png">




        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->
        <link href="../assets/bootstrap-4.5.0/css/bootstrap.min.css" rel="stylesheet">
        <!--    <link href="temas/emokratian/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">-->
        <link href="../temas/demokratian/css/blogStyle.css" rel="stylesheet">
        <!--<link href="temas/emokratian/estilo_login.css" rel="stylesheet">-->
    </head>

    <body>



      <main role="main">
        <div class="container demokratian-access">
            <div class="row">
                <div class="col-lg-6 text-center">

        
          <p>&nbsp;</p>

                        <h2>Bienvenido al actualizador del sistema de votaciones DEMOKRATIAN</h2>
                          <p>Ud. va a actualizar la base de datos a la version 4.0.0</p>
                          <p>Para poder hacerlo anteriormente debe de tener instalada una version de DEMOKRATIAN 2.3.7 o superior</p>

                        <p>
                        <div class="alert alert-info">Recuerde borrar la carpeta<strong> de actualizacion </strong>de su servidor una vez que haya terminado la instalación para evitar problemas.</div>
                        </p>

                    </div>

                <div class="col-lg-6 text-center">

                <p>&nbsp;</p>
                <p>&nbsp;</p>

                    <div class="alert alert-danger">
                    	<p>¡¡¡IMPORTANTE!!! debe de hacer una copia de seguridad su base de datos antes de realizar la actualización</p>
                      </div>
                        <p>
                   			 Si esta seguro, puede actualizar su base de datos a la versión 4</p>

             			   <a href="actualiza_bbdd_4.php" class="btn btn-primary btn-lg active" role="button">Actualizar base de datos</a>

                    </div>

                </div>
            </div>

          </main>
          <div id="footer" class="row">
              <div  class="pie_demokratia">

                  <div class="pie_demokratia2">
                  </div>
                  <div class="pie_demokratia1"> </div>

              </div>
          </div>



    </body>
</html>
