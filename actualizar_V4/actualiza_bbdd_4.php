<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
  require_once("../../private/config/config.inc.php");


  $con = @mysqli_connect("$host", "$hostu", "$hostp") or die("no se puede conectar");
  mysqli_set_charset($con, "utf8");
  $db = @mysqli_select_db($con, "$dbn") or die("no se puede acceder a la tabla");


		$tbn52 = $extension . "blog_categorias_lang";

		$insql1="show tables like '".$tbn52."'";
		$result = mysqli_query($con, $insql1);
		//$result = mysqli_query($con,"show tables like '".$tbn22."'");

		/* Si no existe la tabla */
			if(mysqli_fetch_row($result) == true) {
				$mns='<div class="alert alert-danger">La base de datos ya esta actualizada,al menos en parte y no se puede continuar</div>';
			}else{

       $res = mysqli_query($con, " SELECT SUPPORT FROM INFORMATION_SCHEMA.ENGINES WHERE ENGINE = 'InnoDB'");
         $rowa = mysqli_fetch_assoc($res);

        if($rowa['SUPPORT']=="DEFAULT" or $rowa['SUPPORT']=="YES"){
       $tabla = file_get_contents("actualiza_version_BBDD_V4.sql");

				$tabla = str_replace("demokratian_version_4",$dbn, $tabla);
				$tabla = str_replace("dk_",$extension, $tabla);


				$mens1 ="error al crear la tabla";
				$actualizar_BBDD = mysqli_query($con, $tabla,);

					 if(!$actualizar_BBDD){
             $mns = ' Invalid query:</br>' . mysqli_error($con) . '</br>';
             $mns .= 'Whole query:</br><pre>' . $tabla . '</pre> ';


					 $mns.= '<div class="alert alert-danger">Error al actualizar la base de datos</div>';
					 }else{
					 $mns='<div class="alert alert-success">Se ha actualizado correctamente la base de datos <br/> </div>
          <div class="alert alert-info">Todo ha ido bien, <br/>Recuerde borrar la carpeta<strong> de actualizacion </strong>de su servidor una vez que haya terminado la instalación para evitar problemas.</div>';
					 }
		}else{

         $mns='<div class="alert alert-success">Su servidor no tiene habilitado base de datos  InnoDB. Tiene que actualizar su servidor de BBDD para no tener problemas.</div>';

    }
}

header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
?>
<!DOCTYPE html>
<html lang="es"><head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content=" ">
  <meta name="keywords" content=""/>
  <meta name="language" content="es">
  <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate">
  <meta http-equiv="pragma" CONTENT="no-cache">
  <meta http-equiv="Pragma" content="no-cache">
  <meta http-equiv="Expires" content="0">
        <title>Sistema de instalación de DEMOKRATIAN</title>
        <meta name="author" content="Carlos Salgado">
        <link rel="icon"  type="image/png"  href="../temas/demokratian/images/icono.png">




        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->
        <link href="../assets/bootstrap-4.5.0/css/bootstrap.min.css" rel="stylesheet">
        <!--    <link href="temas/emokratian/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">-->
        <link href="../temas/demokratian/css/blogStyle.css" rel="stylesheet">
        <!--<link href="temas/emokratian/estilo_login.css" rel="stylesheet">-->
    </head>

    <body>



      <main role="main">
        <div class="container demokratian-access">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
          <p>&nbsp;</p>
          <p>&nbsp;</p>
          <p>&nbsp;</p>

                      <p>&nbsp;</p>
                      <p>&nbsp;</p>
                        <h2>Sistema de actualizaciones de DEMOKRATIAN</h2>
                      <p>&nbsp;</p>
                      <p>&nbsp;</p>




                    	<p><?php echo $mns; ?></p>

                        <p>&nbsp;</p>


                </div>
            </div>

        </div>

          <p>&nbsp;</p>
          <p>&nbsp;</p>
          <p>&nbsp;</p>

        </main>

        <div id="footer" class="row">
            <div  class="pie_demokratia">

                <div class="pie_demokratia2">
                </div>
                <div class="pie_demokratia1"> </div>

            </div>
        </div>

    </body>
</html>
