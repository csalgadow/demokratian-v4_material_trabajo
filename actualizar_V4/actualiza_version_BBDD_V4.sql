
-- Servidor: 127.0.0.1
-- Tiempo de generación: 12-12-2020 a las 14:40:44
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=º@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `demokratian_version_4`
--

--
-- Borramos tabla que ya no se usa
--

DROP TABLE IF EXISTS dk_paginas;

--
-- converimos la base de datos a la nueva codificación
--

ALTER DATABASE demokratian_version_4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_520_ci;

--
-- Convertimos las tablas a nueva codifciación
--

ALTER TABLE dk_candidatos CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci;
ALTER TABLE dk_ccaa CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci;
ALTER TABLE dk_codificadores CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci;
ALTER TABLE dk_debate_comentario CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci;
ALTER TABLE dk_debate_preguntas CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci;
ALTER TABLE dk_debate_votos CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci;
ALTER TABLE dk_elvoto CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci;
ALTER TABLE dk_grupo_trabajo CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci;
ALTER TABLE dk_interventores CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci;
ALTER TABLE dk_municipios CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci;
ALTER TABLE dk_nivel_usuario CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci;
ALTER TABLE dk_provincia CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci;
ALTER TABLE dk_usuario_admin_x_provincia CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci;
ALTER TABLE dk_usuario_x_g_trabajo CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci;
ALTER TABLE dk_usuario_x_votacion CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci;
ALTER TABLE dk_votacion CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci;
ALTER TABLE dk_votacion_web CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci;
ALTER TABLE dk_votantes CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci;
ALTER TABLE dk_votantes_x_admin CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci;
ALTER TABLE dk_votos CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci;
ALTER TABLE dk_votos_seg CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci;
ALTER TABLE dk_voto_temporal CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci;

--
-- modificamos las tablas existentes
--
--
-- modificamos los campos de la tabla candidatos
--
ALTER TABLE dk_candidatos MODIFY texto mediumtext NOT NULL;
ALTER TABLE dk_candidatos MODIFY anadido text NOT NULL;
ALTER TABLE dk_candidatos MODIFY fecha_anadido datetime NOT NULL DEFAULT current_timestamp();
ALTER TABLE dk_candidatos MODIFY modif text  DEFAULT NULL;
ALTER TABLE dk_candidatos MODIFY imagen text DEFAULT NULL;
ALTER TABLE dk_candidatos MODIFY imagen_pequena text  DEFAULT NULL;

--
-- modificamos los campos de la tabla codificadores
--

ALTER TABLE dk_codificadores MODIFY nombre text  DEFAULT NULL;
ALTER TABLE dk_codificadores MODIFY correo text  NOT NULL;
ALTER TABLE dk_codificadores MODIFY nif text DEFAULT NULL;
ALTER TABLE dk_codificadores MODIFY pass text  NOT NULL;
ALTER TABLE dk_codificadores MODIFY usuario text  DEFAULT NULL;
ALTER TABLE dk_codificadores MODIFY fecha_ultima datetime DEFAULT current_timestamp();
ALTER TABLE dk_codificadores MODIFY codigo_rec text DEFAULT NULL;
ALTER TABLE dk_codificadores MODIFY clave_publica mediumtext DEFAULT NULL;
ALTER TABLE dk_codificadores MODIFY clave_privada mediumtext DEFAULT NULL;
ALTER TABLE dk_codificadores MODIFY id_votacion int(6) UNSIGNED ZEROFILL NOT NULL;
ALTER TABLE dk_codificadores MODIFY fecha_anadido datetime NOT NULL DEFAULT current_timestamp();
ALTER TABLE dk_codificadores MODIFY fecha_modif datetime DEFAULT current_timestamp();

--
-- modificamos los campos de la dk_debate_comentario
--
ALTER TABLE dk_debate_comentario MODIFY tema text  DEFAULT NULL;
ALTER TABLE dk_debate_comentario MODIFY comentario mediumtext DEFAULT NULL;
ALTER TABLE dk_debate_comentario MODIFY estado text  DEFAULT NULL;

--
-- modificamos los campos de la dk_debate_preguntas
--

ALTER TABLE dk_debate_preguntas MODIFY pregunta text  NOT NULL;
ALTER TABLE dk_debate_preguntas MODIFY anadido int(10) UNSIGNED ZEROFILL NOT NULL;
ALTER TABLE dk_debate_preguntas MODIFY fecha_anadido datetime NOT NULL DEFAULT current_timestamp();
ALTER TABLE dk_debate_preguntas MODIFY modif text  DEFAULT NULL;--
-- modificamos los campos de la dk_debate_pregunta
--

--
-- modificamos los campos de la dk_elvoto
--

ALTER TABLE dk_elvoto MODIFY voto mediumtext  NOT NULL;
ALTER TABLE dk_elvoto MODIFY incluido text  DEFAULT NULL;

--
-- modificamos los campos de la dk_grupo_trabajo
--

ALTER TABLE dk_grupo_trabajo MODIFY subgrupo text NOT NULL;
ALTER TABLE dk_grupo_trabajo MODIFY texto mediumtext  DEFAULT NULL;

--
-- modificamos los campos de la dk_interventores
--

ALTER TABLE dk_interventores MODIFY nombre text DEFAULT NULL;
ALTER TABLE dk_interventores MODIFY apellidos text DEFAULT NULL;
ALTER TABLE dk_interventores MODIFY correo text  NOT NULL;
ALTER TABLE dk_interventores MODIFY anadido text NOT NULL;
ALTER TABLE dk_interventores MODIFY fecha_anadido datetime NOT NULL DEFAULT current_timestamp();
ALTER TABLE dk_interventores MODIFY modif text DEFAULT NULL;
ALTER TABLE dk_interventores MODIFY pass text DEFAULT NULL;
ALTER TABLE dk_interventores MODIFY usuario text  DEFAULT NULL;
ALTER TABLE dk_interventores MODIFY codigo_rec text  DEFAULT NULL;

--
-- modificamos los campos de la dk_nivel_usuario
--

ALTER TABLE dk_nivel_usuario MODIFY  nivel_usuario text NOT NULL;

--
-- modificamos los campos de la dk_provincia
--

ALTER TABLE dk_provincia MODIFY correo_notificaciones text DEFAULT NULL;

--
-- modificamos los campos de la dk_usuario_x_g_trabajo
--

ALTER TABLE dk_usuario_x_g_trabajo MODIFY razon_bloqueo mediumtext DEFAULT NULL;

--
-- modificamos los campos de la dk_usuario_x_votacion
--

ALTER TABLE dk_usuario_x_votacion MODIFY  fecha datetime NOT NULL DEFAULT current_timestamp();
ALTER TABLE dk_usuario_x_votacion MODIFY   correo_usuario text  DEFAULT NULL;
ALTER TABLE dk_usuario_x_votacion MODIFY   ip text NOT NULL;
ALTER TABLE dk_usuario_x_votacion MODIFY   forma_votacion text  NOT NULL;



--
-- modificamos los campos de la dk_votacion
--
ALTER TABLE dk_votacion MODIFY  texto mediumtext DEFAULT NULL;
ALTER TABLE dk_votacion MODIFY resumen mediumtext DEFAULT NULL;
ALTER TABLE dk_votacion MODIFY anadido text NOT NULL;
ALTER TABLE dk_votacion MODIFY fecha_anadido datetime NOT NULL DEFAULT current_timestamp();
ALTER TABLE dk_votacion MODIFY modif text DEFAULT NULL;

--
-- modificamos los campos de la dk_votacion_web
--
ALTER TABLE dk_votacion_web MODIFY imagen_cab  text  DEFAULT NULL;
ALTER TABLE dk_votacion_web MODIFY aux  text  DEFAULT NULL;
ALTER TABLE dk_votacion_web MODIFY diseno mediumtext DEFAULT NULL;

--
-- modificamos los campos de la dk_votantes
--
ALTER TABLE dk_votantes MODIFY nombre_usuario text NOT NULL;
ALTER TABLE dk_votantes MODIFY  apellido_usuario text DEFAULT NULL;
ALTER TABLE dk_votantes MODIFY  correo_usuario text NOT NULL;
ALTER TABLE dk_votantes MODIFY  nif text NOT NULL;
ALTER TABLE dk_votantes MODIFY  pass text DEFAULT NULL;
ALTER TABLE dk_votantes MODIFY  codigo_rec text  DEFAULT NULL;
ALTER TABLE dk_votantes MODIFY  razon_bloqueo text DEFAULT NULL;
ALTER TABLE dk_votantes MODIFY  imagen varchar(191) NOT NULL DEFAULT 'usuario.jpg';
ALTER TABLE dk_votantes MODIFY  imagen_pequena varchar(191)  NOT NULL DEFAULT 'peq_usuario.jpg';
ALTER TABLE dk_votantes MODIFY  perfil mediumtext DEFAULT NULL;

--
-- Añadimos campos de la tabla dk_votantes
--

ALTER TABLE dk_votantes ADD   idioma char(10) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'es_ES';
ALTER TABLE dk_votantes ADD  admin_blog int(2) NOT NULL DEFAULT 0;

--
-- modificamos los campos de la dk_votantes_x_admin
--
ALTER TABLE dk_votantes_x_admin MODIFY fecha datetime NOT NULL DEFAULT current_timestamp();


--
-- modificamos los campos de la dk_votos
--
ALTER TABLE dk_votos MODIFY id_candidato text NOT NULL;
ALTER TABLE dk_votos MODIFY incluido text DEFAULT NULL;
ALTER TABLE dk_votos MODIFY vote_id text  NOT NULL;



--
--  Creamos tablas nuevas
--
--
-- Estructura de tabla para la tabla `dk_acciones`
--

CREATE TABLE IF NOT EXISTS `demokratian_version_4`.`dk_acciones` (
  `ID` int(10) UNSIGNED ZEROFILL NOT NULL,
  `accion` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `tipo_accion` tinytext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `id_user_accion` int(6) UNSIGNED ZEROFILL NOT NULL,
  `fecha_anadido` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci COMMENT='Tabla de las votaciones  existentes';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dk_blog_cabeceras`
--

CREATE TABLE IF NOT EXISTS `demokratian_version_4`.`dk_blog_cabeceras` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_incluido` int(10) UNSIGNED ZEROFILL NOT NULL,
  `titulo` tinytext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `texto` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `fecha` datetime NOT NULL DEFAULT current_timestamp(),
  `imagen` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `id_categoria` smallint(3) NOT NULL,
  `zona_pagina` smallint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dk_blog_categorias`
--

CREATE TABLE IF NOT EXISTS `demokratian_version_4`.`dk_blog_categorias` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_incluido` int(10) UNSIGNED ZEROFILL NOT NULL,
  `titulo` tinytext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `texto` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `fecha` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dk_blog_categorias_lang`
--

CREATE TABLE IF NOT EXISTS `demokratian_version_4`.`dk_blog_categorias_lang` (
  `id` int(10) NOT NULL,
  `id_incluido` int(10) UNSIGNED ZEROFILL NOT NULL,
  `lenguajes_id` int(4) UNSIGNED NOT NULL,
  `blog_categorias_id` int(10) UNSIGNED NOT NULL,
  `titulo` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `texto` mediumtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `cod_idioma` varchar(5) COLLATE utf8mb4_unicode_520_ci DEFAULT 'es_ES'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dk_blog_entradas`
--

CREATE TABLE IF NOT EXISTS `demokratian_version_4`.`dk_blog_entradas` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_incluido` int(10) UNSIGNED ZEROFILL NOT NULL,
  `titulo` tinytext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `texto` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `fecha` datetime NOT NULL DEFAULT current_timestamp(),
  `imagen` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `id_categoria` smallint(3) NOT NULL,
  `zona_pagina` smallint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dk_blog_entradas_lang`
--

CREATE TABLE IF NOT EXISTS `demokratian_version_4`.`dk_blog_entradas_lang` (
  `id` int(10) NOT NULL,
  `id_incluido` int(10) UNSIGNED ZEROFILL NOT NULL,
  `lenguajes_id` int(4) UNSIGNED NOT NULL,
  `blog_entradas_id` int(10) UNSIGNED NOT NULL,
  `titulo` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `texto` mediumtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `cod_idioma` varchar(5) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dk_blog_estructura_inicio`
--

CREATE TABLE IF NOT EXISTS `demokratian_version_4`.`dk_blog_estructura_inicio` (
  `id` int(10) NOT NULL,
  `id_incluido` int(10) UNSIGNED ZEROFILL NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `activo` smallint(1) NOT NULL DEFAULT 0 COMMENT 'si esta activo el bloque 1',
  `bloque` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `orden` smallint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dk_blog_menu`
--

CREATE TABLE IF NOT EXISTS `demokratian_version_4`.`dk_blog_menu` (
  `id` int(10) NOT NULL,
  `id_incluido` int(10) UNSIGNED ZEROFILL NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `nivel` smallint(2) NOT NULL DEFAULT 0,
  `padre` int(10) UNSIGNED ZEROFILL DEFAULT NULL,
  `orden` smallint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dk_blog_menu_lang`
--

CREATE TABLE IF NOT EXISTS `demokratian_version_4`.`dk_blog_menu_lang` (
  `id` int(10) NOT NULL,
  `id_incluido` int(10) UNSIGNED ZEROFILL NOT NULL,
  `lenguajes_id` int(4) UNSIGNED NOT NULL,
  `blog_menu_id` int(10) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `cod_idioma` varchar(5) COLLATE utf8mb4_unicode_520_ci DEFAULT 'es_ES'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dk_blog_paginas`
--

CREATE TABLE IF NOT EXISTS `demokratian_version_4`.`dk_blog_paginas` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_incluido` int(10) UNSIGNED ZEROFILL NOT NULL,
  `titulo` tinytext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `texto` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `fecha` datetime NOT NULL DEFAULT current_timestamp(),
  `borrable` char(2) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'si',
  `pagina` smallint(2) DEFAULT NULL,
  `zona_pagina` smallint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dk_blog_paginas_lang`
--

CREATE TABLE IF NOT EXISTS `demokratian_version_4`.`dk_blog_paginas_lang` (
  `id` int(10) NOT NULL,
  `id_incluido` int(10) UNSIGNED ZEROFILL NOT NULL,
  `lenguajes_id` int(4) UNSIGNED NOT NULL,
  `blog_paginas_id` int(10) UNSIGNED NOT NULL,
  `titulo` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `texto` mediumtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `cod_idioma` varchar(5) COLLATE utf8mb4_unicode_520_ci DEFAULT 'es_ES'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- -----------------------------------------------------------
--
-- Estructura de tabla para la tabla `dk_candidatos_lang`
--

CREATE TABLE IF NOT EXISTS `demokratian_version_4`.`dk_candidatos_lang` (
  `id` int(10) NOT NULL,
  `id_incluido` int(10) UNSIGNED ZEROFILL NOT NULL,
  `lenguajes_id` int(4) UNSIGNED NOT NULL,
  `candidatos_ID` int(6) UNSIGNED ZEROFILL NOT NULL,
  `nombre` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `texto` mediumtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `cod_idioma` varchar(5) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dk_debate_preguntas_lang`
--

CREATE TABLE IF NOT EXISTS `demokratian_version_4`.`dk_debate_preguntas_lang` (
  `id` int(10) NOT NULL,
  `id_incluido` int(10) UNSIGNED ZEROFILL NOT NULL,
  `lenguajes_id` int(4) UNSIGNED NOT NULL,
  `debate_preguntas_ID` int(6) UNSIGNED ZEROFILL NOT NULL,
  `pregunta` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `cod_idioma` varchar(5) COLLATE utf8mb4_unicode_520_ci DEFAULT 'es_ES'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------


--
-- Estructura de tabla para la tabla `dk_grupo_trabajo_lang`
--

CREATE TABLE IF NOT EXISTS `demokratian_version_4`.`dk_grupo_trabajo_lang` (
  `id` int(10) NOT NULL,
  `id_incluido` int(10) UNSIGNED ZEROFILL NOT NULL,
  `lenguajes_id` int(4) UNSIGNED NOT NULL,
  `grupo_trabajo_ID` int(4) UNSIGNED ZEROFILL NOT NULL,
  `subgrupo` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `texto` mediumtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `cod_idioma` varchar(5) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dk_lenguajes`
--

CREATE TABLE IF NOT EXISTS `demokratian_version_4`.`dk_lenguajes` (
  `id` int(4) UNSIGNED NOT NULL,
  `lenguaje_cod_pais_id` int(10) UNSIGNED NOT NULL,
  `cod_LP` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `descripcion` varchar(60) CHARACTER SET utf8 DEFAULT NULL,
  `tipo` int(2) NOT NULL DEFAULT 1 COMMENT 'Para indicar si es el idioma base de la aplicación poner 0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dk_lenguaje_cod_pais`
--

CREATE TABLE IF NOT EXISTS `demokratian_version_4`.`dk_lenguaje_cod_pais` (
  `id` int(10) UNSIGNED NOT NULL,
  `cod_LP` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `descripcion` varchar(60) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------
--
-- Estructura de tabla para la tabla `dk_votacion_lang`
--

CREATE TABLE IF NOT EXISTS `demokratian_version_4`. `dk_votacion_lang` (
  `id` int(10) NOT NULL,
  `votacion_ID` int(6) UNSIGNED ZEROFILL NOT NULL,
  `lenguajes_id` int(4) UNSIGNED NOT NULL,
  `id_incluido` int(10) UNSIGNED ZEROFILL NOT NULL,
  `resumen` mediumtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `texto` mediumtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `cod_idioma` varchar(5) COLLATE utf8mb4_unicode_520_ci DEFAULT 'es_ES'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------
--  creamos los indices y relaciones

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `dk_acciones`
--
ALTER TABLE `dk_acciones`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_dk_acciones_dk_votantes1_idx` (`id_user_accion`);

--
-- Indices de la tabla `dk_blog_cabeceras`
--
ALTER TABLE `dk_blog_cabeceras`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_categoria` (`id_categoria`),
  ADD KEY `fk_dk_blog_entradas_dk_votantes1_idx` (`id_incluido`);

--
-- Indices de la tabla `dk_blog_categorias`
--
ALTER TABLE `dk_blog_categorias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_dk_blog_categrias_dk_votantes1_idx` (`id_incluido`) USING BTREE;

--
-- Indices de la tabla `dk_blog_categorias_lang`
--
ALTER TABLE `dk_blog_categorias_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_dk_categorias_lang_dk_lenguajes_idx` (`cod_idioma`),
  ADD KEY `fk_dk_blog_categorias_lang_dk_lenguajes1_idx` (`lenguajes_id`),
  ADD KEY `fk_dk_blog_categorias_lang_dk_blog_categorias1_idx` (`blog_categorias_id`),
  ADD KEY `fk_dk_blog_categorias_lang_dk_votantes1_idx` (`id_incluido`);

--
-- Indices de la tabla `dk_blog_entradas`
--
ALTER TABLE `dk_blog_entradas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_categoria` (`id_categoria`),
  ADD KEY `fk_dk_blog_entradas_dk_votantes1_idx` (`id_incluido`);

--
-- Indices de la tabla `dk_blog_entradas_lang`
--
ALTER TABLE `dk_blog_entradas_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_idioma` (`cod_idioma`),
  ADD KEY `fk_dk_blog_entradas_lang_dk_blog_entradas1_idx` (`blog_entradas_id`),
  ADD KEY `fk_dk_blog_entradas_lang_dk_lenguajes1_idx` (`lenguajes_id`),
  ADD KEY `fk_dk_blog_entradas_lang_dk_votantes1_idx` (`id_incluido`);

--
-- Indices de la tabla `dk_blog_estructura_inicio`
--
ALTER TABLE `dk_blog_estructura_inicio`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_dk_blog_estructura_inicio_dk_votantes1_idx` (`id_incluido`);

--
-- Indices de la tabla `dk_blog_menu`
--
ALTER TABLE `dk_blog_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_dk_blog_menu_dk_votantes1_idx` (`id_incluido`);

--
-- Indices de la tabla `dk_blog_menu_lang`
--
ALTER TABLE `dk_blog_menu_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_idioma` (`cod_idioma`),
  ADD KEY `fk_dk_blog_menu_lang_dk_blog_menu1_idx` (`blog_menu_id`),
  ADD KEY `fk_dk_blog_menu_lang_dk_lenguajes1_idx` (`lenguajes_id`),
  ADD KEY `fk_dk_blog_menu_lang_dk_votantes1_idx` (`id_incluido`);

--
-- Indices de la tabla `dk_blog_paginas`
--
ALTER TABLE `dk_blog_paginas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_dk_blog_paginas_dk_votantes1_idx` (`id_incluido`);

--
-- Indices de la tabla `dk_blog_paginas_lang`
--
ALTER TABLE `dk_blog_paginas_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_idioma` (`cod_idioma`),
  ADD KEY `fk_dk_blog_paginas_lang_dk_lenguajes1_idx` (`lenguajes_id`),
  ADD KEY `fk_dk_blog_paginas_lang_dk_blog_paginas1_idx` (`blog_paginas_id`),
  ADD KEY `fk_dk_blog_paginas_lang_dk_votantes1_idx` (`id_incluido`);


--
-- Indices de la tabla `dk_candidatos_lang`
--
ALTER TABLE `dk_candidatos_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_idioma` (`cod_idioma`),
  ADD KEY `fk_dk_candidatos_lang_dk_candidatos1_idx` (`candidatos_ID`),
  ADD KEY `fk_dk_candidatos_lang_dk_lenguajes1_idx` (`lenguajes_id`),
  ADD KEY `fk_dk_candidatos_lang_dk_votantes1_idx` (`id_incluido`);


--
-- Indices de la tabla `dk_codificadores`
--
ALTER TABLE `dk_codificadores`
  ADD KEY `fk_dk_codificadores_dk_votantes1_idx` (`anadido`);

--
-- Indices de la tabla `dk_debate_preguntas`
--
ALTER TABLE `dk_debate_preguntas`
  ADD KEY `fk_dk_debate_preguntas_dk_votantes1_idx` (`anadido`);

--
-- Indices de la tabla `dk_debate_preguntas_lang`
--
ALTER TABLE `dk_debate_preguntas_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_idioma` (`cod_idioma`),
  ADD KEY `fk_dk_debate_preguntas_lang_dk_debate_preguntas1_idx` (`debate_preguntas_ID`),
  ADD KEY `fk_dk_debate_preguntas_lang_dk_lenguajes1_idx` (`lenguajes_id`),
  ADD KEY `fk_dk_debate_preguntas_lang_dk_votantes1_idx` (`id_incluido`);


--
-- Indices de la tabla `dk_grupo_trabajo_lang`
--
ALTER TABLE `dk_grupo_trabajo_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_idioma` (`cod_idioma`),
  ADD KEY `fk_dk_grupo_trabajo_lang_dk_lenguajes1_idx` (`lenguajes_id`),
  ADD KEY `fk_dk_grupo_trabajo_lang_dk_grupo_trabajo1_idx` (`grupo_trabajo_ID`),
  ADD KEY `fk_dk_grupo_trabajo_lang_dk_votantes1_idx` (`id_incluido`);


--
-- Indices de la tabla `dk_lenguajes`
--
ALTER TABLE `dk_lenguajes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cod_LP` (`cod_LP`) USING BTREE,
  ADD KEY `fk_dk_lenguajes_dk_lenguaje_cod_pais1_idx` (`lenguaje_cod_pais_id`);

--
-- Indices de la tabla `dk_lenguaje_cod_pais`
--
ALTER TABLE `dk_lenguaje_cod_pais`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx` (`id`),
  ADD UNIQUE KEY `cod_LP` (`cod_LP`) USING BTREE;

--
-- Indices de la tabla `dk_votacion_lang`
--
ALTER TABLE `dk_votacion_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_idioma` (`cod_idioma`),
  ADD KEY `fk_dk_votacion_lang_dk_votacion1_idx` (`votacion_ID`),
  ADD KEY `fk_dk_votacion_lang_dk_lenguajes1_idx` (`lenguajes_id`),
  ADD KEY `fk_dk_votacion_lang_dk_votantes1_idx` (`id_incluido`);


--
-- AUTO_INCREMENT de la tabla `dk_acciones`
--
ALTER TABLE `dk_acciones`
  MODIFY `ID` int(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `dk_blog_cabeceras`
--
ALTER TABLE `dk_blog_cabeceras`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `dk_blog_categorias`
--
ALTER TABLE `dk_blog_categorias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `dk_blog_categorias_lang`
--
ALTER TABLE `dk_blog_categorias_lang`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `dk_blog_entradas`
--
ALTER TABLE `dk_blog_entradas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `dk_blog_entradas_lang`
--
ALTER TABLE `dk_blog_entradas_lang`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `dk_blog_estructura_inicio`
--
ALTER TABLE `dk_blog_estructura_inicio`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `dk_blog_menu`
--
ALTER TABLE `dk_blog_menu`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `dk_blog_menu_lang`
--
ALTER TABLE `dk_blog_menu_lang`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `dk_blog_paginas`
--
ALTER TABLE `dk_blog_paginas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `dk_blog_paginas_lang`
--
ALTER TABLE `dk_blog_paginas_lang`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `dk_candidatos_lang`
--
ALTER TABLE `dk_candidatos_lang`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `dk_debate_preguntas_lang`
--
ALTER TABLE `dk_debate_preguntas_lang`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `dk_grupo_trabajo_lang`
--
ALTER TABLE `dk_grupo_trabajo_lang`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `dk_lenguajes`
--
ALTER TABLE `dk_lenguajes`
  MODIFY `id` int(4) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `dk_lenguaje_cod_pais`
--
ALTER TABLE `dk_lenguaje_cod_pais`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `dk_votacion_lang`
--
ALTER TABLE `dk_votacion_lang`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- Filtros para la tabla `dk_acciones`
--
ALTER TABLE `dk_acciones`
  ADD CONSTRAINT `fk_dk_acciones_dk_votantes1_idx` FOREIGN KEY (`id_user_accion`) REFERENCES `dk_votantes` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `dk_blog_cabeceras`
--
ALTER TABLE `dk_blog_cabeceras`
  ADD CONSTRAINT `fk_dk_blog_entradas_dk_votantes10` FOREIGN KEY (`id_incluido`) REFERENCES `dk_votantes` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `dk_blog_categorias`
--
ALTER TABLE `dk_blog_categorias`
  ADD CONSTRAINT `fk_dk_blog_categorias_dk_votantes1_idx` FOREIGN KEY (`id_incluido`) REFERENCES `dk_votantes` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `dk_blog_categorias_lang`
--
ALTER TABLE `dk_blog_categorias_lang`
  ADD CONSTRAINT `fk_dk_blog_categorias_lang_dk_blog_categorias1` FOREIGN KEY (`blog_categorias_id`) REFERENCES `dk_blog_categorias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_dk_blog_categorias_lang_dk_lenguajes1` FOREIGN KEY (`lenguajes_id`) REFERENCES `dk_lenguajes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_dk_blog_categorias_lang_dk_votantes1` FOREIGN KEY (`id_incluido`) REFERENCES `dk_votantes` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `dk_blog_entradas`
--
ALTER TABLE `dk_blog_entradas`
  ADD CONSTRAINT `fk_dk_blog_entradas_dk_votantes1` FOREIGN KEY (`id_incluido`) REFERENCES `dk_votantes` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `dk_blog_entradas_lang`
--
ALTER TABLE `dk_blog_entradas_lang`
  ADD CONSTRAINT `fk_dk_blog_entradas_lang_dk_blog_entradas1` FOREIGN KEY (`blog_entradas_id`) REFERENCES `dk_blog_entradas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_dk_blog_entradas_lang_dk_lenguajes1` FOREIGN KEY (`lenguajes_id`) REFERENCES `dk_lenguajes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_dk_blog_entradas_lang_dk_votantes1` FOREIGN KEY (`id_incluido`) REFERENCES `dk_votantes` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `dk_blog_estructura_inicio`
--
ALTER TABLE `dk_blog_estructura_inicio`
  ADD CONSTRAINT `fk_dk_blog_estructura_inicio_dk_votantes1` FOREIGN KEY (`id_incluido`) REFERENCES `dk_votantes` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `dk_blog_menu`
--
ALTER TABLE `dk_blog_menu`
  ADD CONSTRAINT `fk_dk_blog_menu_dk_votantes1` FOREIGN KEY (`id_incluido`) REFERENCES `dk_votantes` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `dk_blog_menu_lang`
--
ALTER TABLE `dk_blog_menu_lang`
  ADD CONSTRAINT `fk_dk_blog_menu_lang_dk_blog_menu1` FOREIGN KEY (`blog_menu_id`) REFERENCES `dk_blog_menu` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_dk_blog_menu_lang_dk_lenguajes1` FOREIGN KEY (`lenguajes_id`) REFERENCES `dk_lenguajes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_dk_blog_menu_lang_dk_votantes1` FOREIGN KEY (`id_incluido`) REFERENCES `dk_votantes` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `dk_blog_paginas`
--
ALTER TABLE `dk_blog_paginas`
  ADD CONSTRAINT `fk_dk_blog_paginas_dk_votantes1` FOREIGN KEY (`id_incluido`) REFERENCES `dk_votantes` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `dk_blog_paginas_lang`
--
ALTER TABLE `dk_blog_paginas_lang`
  ADD CONSTRAINT `fk_dk_blog_paginas_lang_dk_blog_paginas1` FOREIGN KEY (`blog_paginas_id`) REFERENCES `dk_blog_paginas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_dk_blog_paginas_lang_dk_lenguajes1` FOREIGN KEY (`lenguajes_id`) REFERENCES `dk_lenguajes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_dk_blog_paginas_lang_dk_votantes1` FOREIGN KEY (`id_incluido`) REFERENCES `dk_votantes` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `dk_candidatos_lang`
--
ALTER TABLE `dk_candidatos_lang`
  ADD CONSTRAINT `fk_dk_candidatos_lang_dk_candidatos1` FOREIGN KEY (`candidatos_ID`) REFERENCES `dk_candidatos` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_dk_candidatos_lang_dk_lenguajes1` FOREIGN KEY (`lenguajes_id`) REFERENCES `dk_lenguajes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_dk_candidatos_lang_dk_votantes1` FOREIGN KEY (`id_incluido`) REFERENCES `dk_votantes` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `dk_codificadores`
--
ALTER TABLE `dk_codificadores`
  ADD CONSTRAINT `fk_dk_codificadores_dk_votantes1` FOREIGN KEY (`anadido`) REFERENCES `dk_votantes` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `dk_debate_preguntas`
--
ALTER TABLE `dk_debate_preguntas`
  ADD CONSTRAINT `fk_dk_debate_preguntas_dk_votacion1` FOREIGN KEY (`id_votacion`) REFERENCES `dk_votacion` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_dk_debate_preguntas_dk_votantes1` FOREIGN KEY (`anadido`) REFERENCES `dk_votantes` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `dk_debate_preguntas_lang`
--
ALTER TABLE `dk_debate_preguntas_lang`
  ADD CONSTRAINT `fk_dk_debate_preguntas_lang_dk_debate_preguntas1` FOREIGN KEY (`debate_preguntas_ID`) REFERENCES `dk_debate_preguntas` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_dk_debate_preguntas_lang_dk_lenguajes1` FOREIGN KEY (`lenguajes_id`) REFERENCES `dk_lenguajes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_dk_debate_preguntas_lang_dk_votantes1` FOREIGN KEY (`id_incluido`) REFERENCES `dk_votantes` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `dk_grupo_trabajo_lang`
--
ALTER TABLE `dk_grupo_trabajo_lang`
  ADD CONSTRAINT `fk_dk_grupo_trabajo_lang_dk_grupo_trabajo1` FOREIGN KEY (`grupo_trabajo_ID`) REFERENCES `dk_grupo_trabajo` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_dk_grupo_trabajo_lang_dk_lenguajes1` FOREIGN KEY (`lenguajes_id`) REFERENCES `dk_lenguajes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_dk_grupo_trabajo_lang_dk_votantes1` FOREIGN KEY (`id_incluido`) REFERENCES `dk_votantes` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `dk_lenguajes`
--
ALTER TABLE `dk_lenguajes`
  ADD CONSTRAINT `fk_dk_lenguajes_dk_lenguaje_cod_pais1` FOREIGN KEY (`lenguaje_cod_pais_id`) REFERENCES `dk_lenguaje_cod_pais` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `dk_votacion_lang`
--
ALTER TABLE `dk_votacion_lang`
  ADD CONSTRAINT `fk_dk_votacion_lang_dk_lenguajes1` FOREIGN KEY (`lenguajes_id`) REFERENCES `dk_lenguajes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_dk_votacion_lang_dk_votacion1` FOREIGN KEY (`votacion_ID`) REFERENCES `dk_votacion` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_dk_votacion_lang_dk_votantes1` FOREIGN KEY (`id_incluido`) REFERENCES `dk_votantes` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;


INSERT INTO `dk_blog_estructura_inicio` (`id`, `id_incluido`, `title`, `description`, `activo`, `bloque`, `orden`) VALUES
  (1, 0000000001, 'Estructura de inicio', 'Orden de los distintos bloques en la página de inicio', 0, '12,15', NULL),
  (11, 0000000001, 'Carrusel Cabecera', 'Cabecera tipo carrusel', 1, 'cabecera-fija.php', NULL),
  (12, 0000000001, 'Cabecera fija', 'Cabecera fija con imagen estatica', 0, 'cabecera-carousel.php', NULL),
  (13, 0000000001, 'Menú principal', 'Menú de navegación', 0, 'navbar.php', NULL),
  (14, 0000000001, 'Menú redes sociales', 'Menú de redes sociales', 0, 'social-navbar.php', NULL),
  (15, 0000000001, 'Login a la web', 'Bloque de acceso a la zona de votaciones', 1, 'bloque_acceso.php', NULL),
  (16, 0000000001, 'Acceso interventores', 'Bloque con el enlace al acceso de los interventores', 0, 'bloque_interventor.php', NULL),
  (18, 0000000001, 'Bloque texto azul', 'Bloque de noticias con fondo azul', 0, 'section-bloktext-blue.php', NULL),
  (19, 0000000001, 'Bloque texto azul', 'Bloque de noticias con fondo blanco', 0, 'section-bloktext.php', NULL),
  (20, 0000000001, 'bloque features', NULL, 0, 'section-features.php', NULL),
  (21, 0000000001, 'Bloque marqueting', NULL, 0, 'section-marqueting.php', NULL);


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
